USE cs125jrr
delete from Persons;
delete from Staff;
delete from Donors;
delete from Requesters;
delete from Kids;


INSERT into Staff (staff_id,person_id,role) VALUES ( 1,1,'Admin');
INSERT into Persons (person_id,email,fname,lname,phone,street,city,state,country,username,password, staff_id,zip) VALUES ( 1,'rodkey@westmont.edu', 'John', 'Rodkey', '805-455-6670', '280 Big Sur Dr', 'Goleta', 'CA', 'USA', 'rodkey', 'mysecretpassword',1,93117);
INSERT into Staff (staff_id,person_id,role) VALUES ( 2,2,'Supervisor');
INSERT into Persons (person_id,email,fname,lname,phone,street,city,state,country,username,password, staff_id,zip) VALUES ( 2,'jerodkey@westmont.edu', 'Jeanne', 'Rodkey', '805-282-4882', '280 Big Sur Dr', 'Goleta', 'CA', 'USA', 'jerodkey', 'hersecretpassword',2,93117);
INSERT into Staff (staff_id,person_id,role) VALUES ( 3,3,'Shelver');
INSERT into Persons (person_id,email,fname,lname,phone,street,city,state,country,username,password, staff_id,zip) VALUES ( 3,'jawright@westmont.edu', 'Janet', 'Wright', '805-929-9292', '123 Some St', 'Carpinteria', 'CA', 'USA', 'jawright', 'hersecretpassword',3,93100);
INSERT into Staff (staff_id,person_id,role) VALUES ( 4,4,'Puller');
INSERT into Donors(donor_id,person_id,needs_receipt) VALUES (1,4,0);
INSERT into Persons (person_id,email,fname,lname,phone,street,city,state,country,username,password, staff_id,zip,donor_id) VALUES ( 4,'khowell@westmont.edu', 'Kay', 'Howell', '805-292-9292', '541 Cold Spring Rd', 'Santa Barbara', 'CA', 'USA', 'khowell', 'hersecretpassword',4,94100,1);
INSERT into Kids ( requester_id,child_id,name,birth_year,reading_level,dt_updated ) VALUES (1,1,'Nelson','2010-01-01','advanced','2019-08-19');
INSERT into Kids ( requester_id,child_id,name,birth_year,reading_level,dt_updated ) VALUES (1,2,'Elia','2012-01-01','advanced','2019-08-19');
INSERT into Kids ( requester_id,child_id,name,birth_year,reading_level,dt_updated ) VALUES (1,3,'Joel','2014-01-01','beginning','2019-08-19');
INSERT into Requesters (requester_id,person_id,lt_id,how_heard) VALUES ( 1,5,'BBKinPNG','friends');
INSERT into Persons (person_id,email,fname,lname,phone,street,city,state,country,username,password, staff_id,zip,requester_id) VALUES (5, 'skurz@maf.org', 'Betty', 'Kurz', NULL, '281 Western Province', 'Mount Hagen', NULL, 'Papua New Guinea', 'bkurz', 'hersecretpassword',5,NULL,1);
