 #!/bin/bash

# manipulate raw bookends database csv files to something more manageable

# on librarything.com, do a complete export of the books in tsv form.  
# This is 47000 books, and takes about 1 hour to export.
# mv ~/Downloads/librarything_BookEndsIntl.tsv LTraw.tsv

# create book files for those with and without donors
grep 'From:'    LTtrimmed.tsv >LTdonors.tsv
grep -v 'From:' LTtrimmed.tsv >LTnodonor.tsv

# create the list of donor userids
cut -d'	' -f17 LTdonors.tsv | fmt -1 | grep From: | tr -d ',"' | sort -u | sed 's/From://' >donoruserids.tsv

# create donors.tsv with fields donor_id, donoruserid, donor_fname, donor_lname
# assumes camelcase donoruserid

sed 's/\([a-z]\)\([A-Z]\)/\1 \2/g' < donoruserids.tsv |
 awk '{ print NR-1"	"$1$2$3"	"$0}' | sed 's/ /	/' > donors.tsv
# create the list of recipient userids
cut -d'	' -f17 LTtrimmed.tsv | sed 's/#reserved /To:/' | sed 's/#reserve /To:/' | fmt -1 | grep To: | tr -d ',"' | sort -u | sed 's/To://' >recipientuserids.tsv

# create recipients.tsv with fields recipient_id, recipientuserid, recipient_fname, recipient_lname
# assumes camelcase recipientuserid

sed 's/\([a-z]\)\([A-Z]\)/\ \2/g' < recipientuserids |
 awk '{ print NR-1"	"$1$2$3"	"$0}' | sed 's/ /	/' > recipients.tsv

# create a file for each donor containing each book donated by them
for i in `cat donoruserids` 
do 
   grep -w $i LTtrimmed.tsv  >donors/$i.tsv
done

# download the BookEnds Order database locally, move it to the current folder and name it orders.tsv
# mv ~/Downloads/2018\ BookEnds\ Database\ -\ Sheet1.tsv orders.tsv

cp orders.tsv ordersraw.tsv

#ignore unneeded fields
cut -d'	' -f 1-18,25,26 <orders.tsv >orders2.tsv 


# only deal with orders that are associated with a known recipient ID
grep -if recipientuserids  orders2.tsv >orders.tsv 
rm orders2.tsv

# create list of donations (combo of donoruserid and date added)
rm donations.tsv
for u in `cat donoruserids`
do
    for d in `grep -w "From:$u" LTtrimmed.tsv | cut -d'	' -f 23 | sort -u`
    do
         echo "$u	$d" >>donations.tsv
    done
done
for i in `cut -d'	' -f 23 LTnodonor.tsv  | sort -u `
do 
    echo "Unknown	$i" >>donations.tsv 
done

sort -u donations.tsv >/tmp/$$
mv /tmp/$$ donations.tsv

# create a file containing LT Book ID with all  tags
cut -d'	' -f 1,17,23 <LTtrimmed.tsv >ltid_tags.tsv

# create a file listing the LT Book Id, donoruserid and recipientuserid

# make a file listing donations but using the donor_id integer instead of donoruserid

rm donationsbyid.tsv
let j=0
for u in `awk '{print $1}' donations.tsv|sort -u `
do 
   did=`grep  "	$u	" donors.tsv | awk '{print $1}'`
   for d in `grep -w $u donations.tsv| awk '{print $2}'`
   do 
     let j=j+1 
     echo "$j	$did	$d" >>donationsbyid.tsv
   done
done
