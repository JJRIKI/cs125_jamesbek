CREATE TABLE People (
	person_id.varchar(80) UNIQUE auto_increment,
	username.varchar(80),
	password.varchar(80),
	first_name.varchar(80),
	last_name.varchar(80),
	email.varchar(80),
	phone_number.int,
	PRIMARY KEY (person_id)
);

CREATE TABLE Staff (
	staff_id.int UNIQUE auto_increment,
	role.varchar(80),
	PRIMARY KEY (staff_id),
	FOREIGN KEY (person_id) REFERENCES People(person_id)
);

CREATE TABLE Donor (
	donor_id.int UNIQUE auto_increment,
	recv_receipt.BIT,
	PRIMARY KEY (donor_id)
	FOREIGN KEY (person_id) REFERENCES People(person_id)
);

CREATE TABLE Donation (
	date_donated.datetime,
	donation_type.varchar(80),
	amount_donated.int,
	FOREIGN KEY (book_id) REFERENCES Book(book_id),
	FOREIGN KEY (donor_id) REFERENCES Donor(donor_id)
);

CREATE TABLE Client (
client_id.int UNIQUE auto_increment,
	how_found.varchar(9000),
	LT_id.int UNIQUE,
	PRIMARY KEY (client_id),
	FOREIGN KEY (request_id) REFERENCES Request(request_id),
	FOREIGN KEY (person_id) REFERENCES People(person_id)
);

CREATE TABLE Child (
	birth_year.year,
	name.varchar(80),
	interests.varchar(9000),
	reading_level.varchar(80),
	last_update.datetime,
	FOREIGN KEY (client_id) REFERENCES Client(client_id)
);

CREATE TABLE Request (
	request_id.int UNIQUE auto_increment,
	num_books.int,
	request_date.datetime,
	special_request.varchar(9000),
	PRIMARY KEY (request_id)
);

CREATE TABLE Location (
	delivery_method.int,
	country.varchar(80),
	address.varchar(80),
	FOREIGN KEY (request_id) REFERENCES Request(request_id)
);

CREATE TABLE Orders (
	order_status.varchar(80),
	order_stage.varchar(80),
	num_boxes.int(9),
	box_size.varchar(80),
	payment_total.int,
	payment_payed.int(,
	FOREIGN KEY (request_id) REFERENCES Book(request_id),
	FOREIGN KEY (book_id) REFERENCES Book(book_id)
);

CREATE TABLE Book (
	book_id.int UNIQUE auto_increment,
	google_id.int UNIQUE,
	LT_id.int,
	title.varchar(80),
	date_published.datetime,
	edition.varchar(80),
	status.varchar(80),
	ISBN_10.int,
	ISBN_13.int,
	PRIMARY KEY (book_id)
);

CREATE TABLE Work (
	work_id.int UNIQUE auto_increment,
	google_id.int UNIQUE,
	author.varchar(80),
	publisher.varchar(80),
	work_title(80),
	ISBN_10.int,
	ISBN_13.int,
	PRIMARY KEY (work_id),
	FOREIGN KEY (book_id) REFERENCES Book(book_id)
);

