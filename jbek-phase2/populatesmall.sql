INSERT INTO Persons (person_id, username, password, first_name, last_name, email, phone_number) VALUES (1, jrodkey, password1, John, Rodkey, rodkey@westmont.edu, 8055656177);
INSERT INTO Persons (person_id, username, password, first_name, last_name, email, phone_number) VALUES (2, jbek, password2, James, Bek, jbek@westmont.edu, 4087816056);
INSERT INTO Persons (person_id, username, password, first_name, last_name, email, phone_number) VALUES (3, nyoung, password3, Nathan, Young, nyoung@westmont,edu, 9163808923);
INSERT INTO Persons (person_id, username, password, first_name, last_name, email, phone_number) VALUES (4, rfrink, password4, Rebecca, Frink, rfrink@westmont.edu, 8183215450);
INSERT INTO Persons (person_id, username, password, first_name, last_name, email, phone_number) VALUES (5, kegao, password5, Kevin, Gao, kegao@westmont.edu, 8055705460);
INSERT INTO Persons (person_id, username, password, first_name, last_name, email, phone_number) VALUES (6, jdoe, password7, John, Doe, jdoe@anonymous.org, 0000000000);

INSERT INTO Staff (staff_id, role, person_id) VALUES (1, 'Admin', 1);
INSERT INTO Staff (staff_id, role, person_id) VALUES (2, 'Developper', 2);
INSERT INTO Staff (staff_id, role, person_id) VALUES (3, 'Developper', 3);
INSERT INTO Staff (staff_id, role, person_id) VALUES (4, 'Developper', 4);
INSERT INTO Staff (staff_id, role, person_id) VALUES (5, 'Developper', 5);

INSERT INTO Client (1, 'From John Rodkey', 0000000000, 6);

INSERT INTO Donor (donor_id, recv_receipt, person_id) VALUES (1, 0, 6);

INSERT INTO Donation (date_donated, donation_type, amount_donated) VALUES (2019-10-03, 'Money', 100);

INSERT INTO Child (birth_year, name, interests, reading_level, last_update) VALUES (2010, 'Billy', 'Sci-fi or fantasy books', 'Young Adult Ficton level', 2019-10-03 15:04:01);

INSERT INTO Request (request_id, num_books, request_date, special_request) VALUES (1, 1, 2019-10-03 15:05:23, 'Hardcover');

INSERT INTO Location (1. 'South Korea', "(대한민국)
서울특별시 종로구 사직로3길 23,
102동 304호
홍길동 귀하
30174");

INSERT INTO Orders (order_status, order_stage, num_boxes,box_size, payment_total, payment_payed) VALUES (0, 1, 1, 'small', 5, 5);

